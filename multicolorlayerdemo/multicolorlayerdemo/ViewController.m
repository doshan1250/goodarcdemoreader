//
//  ViewController.m
//  MulticolorLayerDemo
//
//  Created by Liuyu on 14-7-5.
//  Copyright (c) 2014年 Liuyu. All rights reserved.
//

#import "ViewController.h"
#import "MulticolorView.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <AVFoundation/AVFoundation.h>

#define LOCK_DURATION 15

@interface ViewController ()<CBCentralManagerDelegate, CBPeripheralDelegate,UITableViewDelegate,UITableViewDataSource>
{    
    __weak IBOutlet UIView *centerView;
    MulticolorView *circleView;
    NSMutableDictionary *targetDic;
    NSMutableDictionary *waitingKey;
    int lockStatus;
    __weak IBOutlet UIImageView *lockImage;
    AVAudioPlayer *player;
    
    CALayer *_layer;
    CAAnimationGroup *_animaTionGroup;
    CADisplayLink *_disPlayLink;
    
    NSMutableArray *dataArray;
    
    NSDateFormatter *f;
    
    NSTimeInterval countDownInterval;
    NSTimer *countDownTImer;
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_duration;
@property (weak, nonatomic) IBOutlet UILabel *lbl_log;
@property (strong, nonatomic) CBCentralManager  *centralManager;
@property (strong, nonatomic) CBPeripheral  *discoveredPeripheral;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *sliderValueLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    f = [NSDateFormatter new];
    [f setDateFormat:@"HH:mm:ss"];
    
    dataArray = [NSMutableArray new];
    //圈圈
    circleView = [[MulticolorView alloc] initWithFrame:centerView.frame];
    [circleView startAnimation];
    [centerView addSubview:circleView];
    
    //初始化
    targetDic = [NSMutableDictionary new];
    waitingKey = [NSMutableDictionary new];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"beep-04" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:path];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorStateMonitor:) name:@"UIDeviceProximityStateDidChangeNotification" object:nil];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    //IR sensor
    NSLog(@"設備%i用",[UIDevice currentDevice].proximityMonitoringEnabled);
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    [self updateSliderValue];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellll"];
}

-(void) fireTimer{
    if (countDownTImer.isValid) {
        [countDownTImer invalidate];
    }
    countDownInterval = LOCK_DURATION;
    self.lbl_duration.text = [@(countDownInterval) stringValue];
    
    countDownTImer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown:) userInfo:nil repeats:true];
}
-(void) stopTimer{
    [countDownTImer invalidate];
}
-(void) countDown:(id)timer{
    countDownInterval --;
    
    self.lbl_duration.text = [@(countDownInterval) stringValue];
    
    if (countDownInterval == 0) {
        [self stopTimer];
        [self setLockStatus:0];
    }
}

- (IBAction)valueChange:(id)sender {
    
    self.slider.value = roundf(self.slider.value);
    
    
    [self updateSliderValue];
}

-(void) updateSliderValue{
    
    NSDecimalNumberHandler *round = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundPlain
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    NSString *strValue = [[[[NSDecimalNumber alloc] initWithFloat:self.slider.value] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc] initWithFloat:-0.036]] decimalNumberBySubtracting:[[NSDecimalNumber alloc] initWithFloat:1.34] withBehavior:round].stringValue;
    
    self.sliderValueLabel.text = [NSString stringWithFormat:@"約 %@公尺",strValue];
}

- (void)setLockStatus:(int)new_locakStatus{
    
    if (new_locakStatus == 1) {
        [self fireTimer];
    }
    
    if (new_locakStatus != lockStatus) {
        lockStatus = new_locakStatus;
        [self playBeep];
        if (lockStatus == 0) {
            [lockImage  setImage:[UIImage imageNamed:@"locked.png"]];
        }else{
            [lockImage  setImage:[UIImage imageNamed:@"unlocked.png"]];
        }
    }
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self scan];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)scan{
    
    if (self.discoveredPeripheral) {
        [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
        
    }
    [self.centralManager stopScan];
    
    [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:@"83f8b543-1dea-470a-a398-f700a550b5c6"]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
}
- (void)sensorStateMonitor:(NSNotificationCenter *)notification
{
    [[UIScreen mainScreen] setBrightness:1.0];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    if ([[UIDevice currentDevice] proximityState] == YES)
    {
        NSLog(@"Device is close to user.");
    }
    
    else
    {
        NSLog(@"Device is not closer to user.");
    }
}
#pragma mark - Table View delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellll"];
    
    cell.textLabel.text = dataArray[indexPath.row];

    return cell;
}

#pragma mark - BLE callBacks
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBCentralManagerStatePoweredOn) {
        // In a real app, you'd deal with all the states correctly
        return;
    }
    
    // The state must be CBCentralManagerStatePoweredOn...
    
    // ... so start scanning
    [self scan];
    
}
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSString *keyString = [peripheral.identifier UUIDString];
    NSLog(@"discove %@ %d",keyString,[RSSI intValue]);
    
    self.lbl_log.text = [NSString stringWithFormat:@"discove %@ %d",keyString,[RSSI intValue]];
    if (keyString == nil) {
        return;
    }
    if (waitingKey[keyString] && [waitingKey[keyString] boolValue] == NO) {
        return;
    }

    if ([RSSI intValue] > self.slider.value && [RSSI intValue] < -20) {
        
        if (!targetDic[keyString]) {
            [targetDic setObject:[NSNumber numberWithInt:1] forKey:keyString];
        }else{
            int currentCount = [targetDic[keyString] intValue];
            ++currentCount;
            NSLog(@"%@ %d",keyString,currentCount);
            if (currentCount > 2) {
                
                
                //Lunch Open
                for (CBPeripheral *u in [self.centralManager retrieveConnectedPeripheralsWithServices:@[[CBUUID UUIDWithString:@"83f8b543-1dea-470a-a398-f700a550b5c6"]]]) {
                    NSLog(@"retrieve UUID :%@",u.identifier.UUIDString);
                }
                if (self.discoveredPeripheral != peripheral) {
                    self.discoveredPeripheral = peripheral;
                    [targetDic setObject:[NSNumber numberWithInt:0] forKey:keyString];
                    [self.centralManager connectPeripheral:peripheral options:nil];
                }
            }else{
                //繼續等...
                [targetDic setObject:[NSNumber numberWithInt:currentCount] forKey:keyString];
            }
        }
    }
    else{
        if (targetDic[keyString]) {
            [targetDic setObject:[NSNumber numberWithInt:0] forKey:keyString];
        }
    }
    
}
-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"Peripheral didFailToConnectPeripheral");
    
    if ([self.discoveredPeripheral isEqual:peripheral]) {
        [self.centralManager cancelPeripheralConnection:peripheral];
        self.discoveredPeripheral = nil;
    }
    [self scan];
    
}
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    NSLog(@"Peripheral Connected");
    
    peripheral.delegate = self;
    
    // Search only for services that match our UUID
    [peripheral discoverServices:@[[CBUUID UUIDWithString:@"83f8b543-1dea-470a-a398-f700a550b5c6"]]];
}
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        [self.centralManager cancelPeripheralConnection:peripheral];
        [self scan];
        return;
    }
    //
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:@"09c72062-c13a-4e82-a803-b17f58a1f632"]] forService:service];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    NSString *dataFromPeripheral = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
    NSLog(@"Get Value from p :%@",dataFromPeripheral);
    
    if (waitingKey[peripheral.identifier.UUIDString] && [waitingKey[peripheral.identifier.UUIDString] boolValue] == NO) {
        [self.centralManager cancelPeripheralConnection:peripheral];
        [self scan];
        return;
    }
    // 例外處理1 : app第一次被系統喚醒會有Unknow ATT error, 先過濾掉
    if (dataFromPeripheral == nil || dataFromPeripheral.length == 0) {
        [self.centralManager cancelPeripheralConnection:peripheral];
        [self scan];
        return;
    }
    
    [dataArray addObject:[NSString stringWithFormat:@"%@ 使用者: %@",[f stringFromDate:[NSDate date]],dataFromPeripheral]];
    [self.tableview reloadData];
    [self.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:dataArray.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    //拿完資料踢走你
    [self.centralManager cancelPeripheralConnection:peripheral];
    
    //Animation
    [self eventBlueLight];
    
    //靜待15秒
    [self setLockStatus:1];
    [waitingKey setObject:[NSNumber numberWithBool:NO] forKey:peripheral.identifier.UUIDString];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, LOCK_DURATION * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [waitingKey setObject:[NSNumber numberWithBool:YES] forKey:peripheral.identifier.UUIDString];
    });
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        return;
    }
    for (CBCharacteristic *characteristic in service.characteristics) {
        [peripheral readValueForCharacteristic:characteristic];
    }
    
}
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"Peripheral Disconnected");
    
    if ([peripheral isEqual:self.discoveredPeripheral]) {
        self.discoveredPeripheral = nil;
    }
    [self scan];
}

- (void)playBeep{
    [player play];
}

#pragma mark - Animation
- (void)startAnimation
{
    CALayer *layer = [[CALayer alloc] init];
    layer.cornerRadius = [UIScreen mainScreen].bounds.size.width/2;
    CGRect frameInSelfView = [circleView convertRect:circleView.bounds toView:self.view];
    layer.frame = CGRectMake(0, 0, layer.cornerRadius * 2, layer.cornerRadius * 2);
    layer.position = CGPointMake(self.view.frame.size.width/2, frameInSelfView.origin.y + frameInSelfView.size.height/2);
    UIColor *color = [UIColor colorWithRed:175/255.0 green:255/255.0 blue:161/255.0 alpha:1.0];
    layer.backgroundColor = color.CGColor;
    [self.view.layer addSublayer:layer];
    
    CAMediaTimingFunction *defaultCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    
    _animaTionGroup = [CAAnimationGroup animation];
    _animaTionGroup.delegate = self;
    _animaTionGroup.duration = 2;
    _animaTionGroup.removedOnCompletion = YES;
    _animaTionGroup.timingFunction = defaultCurve;
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
    scaleAnimation.fromValue = @0.0;
    scaleAnimation.toValue = @1.0;
    scaleAnimation.duration = 2;
    
    CAKeyframeAnimation *opencityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    opencityAnimation.duration = 2;
    opencityAnimation.values = @[@0.8,@0.4,@0];
    opencityAnimation.keyTimes = @[@0,@0.5,@1];
    opencityAnimation.removedOnCompletion = YES;
    
    NSArray *animations = @[scaleAnimation,opencityAnimation];
    _animaTionGroup.animations = animations;
    [layer addAnimation:_animaTionGroup forKey:nil];
    
    [self performSelector:@selector(removeLayer:) withObject:layer afterDelay:1.5];
    [self.view bringSubviewToFront:centerView];
}

- (void)removeLayer:(CALayer *)layer
{
    [layer removeFromSuperlayer];
}

- (void)eventBlueLight
{
    if (!_disPlayLink) {
        [self delayAnimation];
//        _disPlayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(delayAnimation)];
//        _disPlayLink.frameInterval = 120;
//        [_disPlayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
}

- (void)delayAnimation
{
    [self startAnimation];
}
@end
